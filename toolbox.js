/*
This is a library of simple, everyday functions, which should simplify life,
without breaking the bank. No need more to whip out JQuery to hide a button.

Style rules:
* Global Variables: PascalCase.
* Functions and local variables (arguments): camelCase.
* Function names should indicate their purpose.
* Use semicolons. JS doesn't need em, but here, we use em.

***/

/*Globals*/
var date = new Date();
var CurrentYear = date.getFullYear();

//This has not been tested yet:
var $ = function(id) {
  return document.getElementById(id);
}

//History related functions
function navBack() {
  window.history.back();
}

function navForward() {
  window.history.forward();
}

/***Date related functions***/

//Grab the year.
function getYear(id) {
  document.getElementById(id).innerHTML = date.getFullYear();
}

//return the year.
function returnYear() {
  return CurrentYear;
}

function placeYearDifference(input, target) {
  outputto = document.getElementById(target);
  outputto.innerHTML = calcYearDifference(input);
}

//Calculate the number of years between the current date and another year.
function calcYearDifference(num) {
  let result = CurrentYear - num;
  return result;
}

//Set the content to a set of text according to a variable.
function changeContent(loc, content) {
  document.getElementById(loc).innerHTML = content;
}

//Load an HTML file from the server.
function loadContent(loc, descSRC) {
  let location = document.getElementById(loc);
  let content = new XMLHttpRequest();
  /*
    TODO: add a check to see if it exists or not.
    So far, none of the proposed solutions on SE work.
  */
  content.open('GET', descSRC, true);
  content.onreadystatechange = function() {
    if (this.readyState!==4) return;
    if (this.status!==200) return;
    location.innerHTML = this.responseText;
  };
  content.send();
}

//Like loadContent, but in reverse - basically, blank an area to save resources.
function unloadContent(loc) {
  document.getElementById(loc).innerHTML = "";
}

//toggleClass - turn a class on and off.
function toggleClass(element, classy) {
  let actor = document.getElementById(element);
  if (actor.classList.contains(classy)) {
    actor.classList.remove(classy);
  }
  else {
    actor.classList.add(classy);
  }
}

//Add/Remove CSS Classes:
// TODO: make it so that these functions check for the class they're acting on.
function addClass(element, classy) {
  let actor = document.getElementById(element);
  actor.classList.add(classy);
}

function removeClass(element, classy) {
  let actor = document.getElementById(element);
  actor.classList.remove(classy);
}

//Check for a class
function classCheck(element, classy) {
  let actor = document.getElementById(element);
  if (actor.classList.contains(classy)){
    return true;
  }
  else {
    return false;
  }
}

//Attribute functions:
function grabAttribute(element, attr) {
  let actor = document.getElementById(element);
  let result = actor.getAttribute(attr);
  return result;
}

// TODO: Fix the name of this function O_o
function setAttribute(element, attr, info) {
  let actor = document.getElementById(element);
  actor.setAttribute(attr, info);
}

function changeAttribute(element, attr, info) {
  let actor = document.getElementById(element);
  actor.setAttribute(attr, info);
}

function hide(element) {
  document.getElementById(element).style.display = 'none';
  console.log("Element hidden: " + element + ".")
}

function hideMe(element) {
  element.style.display = 'none';
  console.log("Element hidden: " + element + ".")
}

// TODO: dontScroll() - stop the body from scrolling when run.
//Scroll Functions
function ScrollUpTop() {
  window.scrollTo({top: 0, behavior: 'smooth'});
}

function ScrollElementUp(element) {
  let target = document.getElementById(element);
  target.scrollTop = 0;
}

function ScrollElementUpSmooth(element) {
  let target = document.getElementById(element);
  target.scrollTo({top: 0, behavior: 'smooth'});
}

// TODO: Add true tab related functions.
// PRIORITY^
function tabSwitcher(TabList, TabContent, test) {
  //Trying to figure this one out.
  //Needs review and testing.
  //tab() might work, once I finish it, and requires far less code.

  let select = test.getAttribute('data-target');
  let content = document.getElementsByClassName(TabContent);
  let list = document.getElementsByClassName(TabList);
  let target = document.getElementById(select);

  //Hide all tabs
  for (i = 0; i < content.length; i++) {
    content[i].classList += " noplay";
  }

  //Reset tab list
  for (i = 0; i < list.length; i++) {
    list[i].classList -= " active";
  }

  //Show the selected tab - does not work yet:
  removeClass(test.getAttribute('data-target'), "noplay");
  console.log("Showing " + test.getAttribute('data-target'));
}
